import { Selector, t } from "testcafe";

import { Alice } from "../roles";

const registerTestUsers = async () => {
  await t
      .navigateTo(`http://localhost:3000/register.html`)
      .typeText("#register-username", "alice")
      .typeText("#register-password", "aliceisok")
      .typeText("#register-email", "tester@testing.com")
      .click("#btn-create-account");
}

fixture `Register`
    .page`http://localhost:3000/register.html`;

test('Can register', async t => {
   await registerTestUsers();
})

fixture`Login`
    .page`http://localhost:3000/login.html`
    .beforeEach(async (t) => {
      await registerTestUsers();
      await t.useRole(Alice);
    });

test("Can view profile", async (t) => {

  const username = Selector("#username", { timeout: 1000 });
  const textContent = username.textContent;

  await t
    .navigateTo("/profile.html")
    .expect(username.exists).ok()
//    .expect(textContent).eql("alice");
});
