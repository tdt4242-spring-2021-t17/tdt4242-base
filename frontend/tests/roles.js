import { Role } from "testcafe";

const Alice = Role(`http://localhost:3000/login.html`, async (t) => {
    await t
        .typeText("#login-username", "alice")
        .typeText("#login-password", "aliceisok")
        .click("#rememberMe")
        .click("#btn-login");
});

export { Alice }