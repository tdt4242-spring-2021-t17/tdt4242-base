/**
 *
 * @param {string} elementID The ID of the element to show
 */
function showElement(elementID) {
  document.getElementById(elementID).classList.remove("hide");
}

/**
 *
 * @param {string} elementID The ID of the element to hide
 */
function hideElement(elementID) {
  document.getElementById(elementID).classList.add("hide");
}

/**
 * 
 * @param {string} textID 
 * @param {string} rowID 
 * @param {string} value 
 */
function displayRow(textID, rowID, value) {
  if (value) {
    document.getElementById(textID).textContent = value;
    showElement(rowID);
  }
}

/**
 *
 * @param {{username: String, id: Number, url: String, email: String, phone_number: String, country: String, city: String, street_address: String, bio: String, athletes: []}} profile
 */
function renderProfileDetails(profile) {
  if (profile.profile_image != null) {
    document.getElementById("profile-picture").src = profile.profile_image;
  }
  document.getElementById("username").textContent = profile.username;
  document.getElementById("email").textContent = profile.email;
  displayRow("phone", "phone-row", profile.phone_number);
  displayRow("country", "country-row", profile.country);
  displayRow("city", "city-row", profile.city);
  displayRow("address", "address-row", profile.street_address);
  document.getElementById("bio").textContent = profile.bio;
}

/**
 * @param {{username: String, id: Number, url: String, email: String, phone_number: String, country: String, city: String, street_address: String, bio: String, athletes: []}} profile
 */
function switchToEditBio(profile) {
  const bioEdit = document.getElementById("bio-edit");
  bioEdit.value = profile.bio;
  bioEdit.classList.remove("hide");
  hideElement("bio-display");
}

function switchToDisplayBio() {
  hideElement("bio-edit");
  showElement("bio-display");
}

function editImage() {
  hideElement("btn-image-edit");
  showElement("div-image-edit");
}

function displayImage() {
  hideElement("div-image-edit");
  showElement("btn-image-edit");
}

/**
 *
 * @param {{username: String, id: Number, url: String, email: String, phone_number: String, country: String, city: String, street_address: String, bio: String, athletes: []}} profile
 * @param {Boolean} isCurrentUser
 */
function renderProfile(profile, isCurrentUser) {
  renderProfileDetails(profile);
  const editBioButton = document.getElementById("btn-bio-edit");
  const editImageButton = document.getElementById("btn-image-edit");
  document.getElementById("bio-edit").classList.add("hide");
  if (isCurrentUser) {
    editBioButton.addEventListener("click", () => switchToEditBio(profile));
    document
      .getElementById("btn-cancel-bio")
      .addEventListener("click", () => switchToDisplayBio());
    document
      .getElementById("btn-save-bio")
      .addEventListener("click", () => saveBio(profile));
    editImageButton.classList.remove("hide");
    editImageButton.addEventListener("click", () => editImage());
    document
      .getElementById("btn-save-image")
      .addEventListener("click", (event) => saveImage(event, profile));
    document
      .getElementById("btn-cancel-image")
      .addEventListener("click", () => displayImage(profile));
  } else {
    document.getElementById("btn-add-friend").classList.remove("hide");
    editBioButton.classList.add("hide");
  }
}

function generateProfileImageForm() {
  const form = document.getElementById("image-form");
  const formData = new FormData(form);
  const file = formData.get("files");
  const fileNameEnding = file.name.split(".")[1];
  const fileType = file.type;
  const blob = file.slice(0, file.size, fileType);
  const newFile = new File([blob], `profile_image.${fileNameEnding}`, {
    type: fileType,
  });
  if (newFile.size > 0) {
    const submitForm = new FormData();
    submitForm.append("profile_image", newFile);
    return submitForm;
  }
}

/**
 *
 * @param {MouseEvent} event
 * @param {{username: String, id: Number, url: String, email: String, phone_number: String, country: String, city: String, street_address: String, bio: String, athletes: []}} profile
 */
async function saveImage(event, profile) {
  const form = generateProfileImageForm();
  const response = await sendRequest(
    "PATCH",
    `${HOST}/api/users/${profile.id}/`,
    form,
    ""
  );
  if (response.ok) {
    const data = await response.json();
    console.log(data);
    location.reload();
  } else {
    const data = await response.json();
    const alert = createAlert("Could not upload file!", data);

    document.body.prepend(alert);
  }
}

/**
 *
 * @param {{username: String, id: Number, url: String, email: String, phone_number: String, country: String, city: String, street_address: String, bio: String, athletes: []}} profile
 */
async function saveBio(profile) {
  const newBio = document.getElementById("textarea-bio-edit").value;
  const form = new FormData();
  form.append("bio", newBio);
  const response = await sendRequest("PATCH", profile.url, form, "");
  if (response.ok) {
    location.reload();
  } else {
    const data = await response.json();
    const alert = createAlert("Could not upload bio!", data);

    document.body.prepend(alert);
  }
}

/**
 * @returns [user: User, isCurrentUser: Boolean]
 */
async function getProfile() {
  const urlParams = new URLSearchParams(window.location.search);
  const currentUser = await getCurrentUser();
  if (urlParams.has("id")) {
    const id = Number(urlParams.get("id"));
    if (id === currentUser.id) {
      return [currentUser, true];
    }
    let user = null;
    const response = await sendRequest("GET", `${HOST}/api/users/${id}/`);
    if (!response.ok) {
      console.log(`COULD NOT RETRIEVE USER ${id}`);
    } else {
      const data = await response.json();
      user = data;
    }
    return [user, false];
  } else {
    return [currentUser, true];
  }
}

window.addEventListener("DOMContentLoaded", async () => {
  const [profile, isCurrentUser] = await getProfile();
  renderProfile(profile, isCurrentUser);
});
