/**
 *
 * @param {String} username
 * @returns {Promise<{username: String, id: Number, url: String, email: String, phone_number: String, country: String, city: String, street_address: String, bio: String, athletes: []}>}
 */
async function fetchSearchResult(username) {
  const response = await sendRequest(
    "GET",
    `${HOST}/api/users/?search=${username}`
  );

  if (response.ok) {
    const data = await response.json();
    const users = data.results;
    return users;
  } else {
    const data = await response.json();
    const alert = createAlert("Could not search for users", data);
    document.body.prepend(alert);
  }
}

/**
 *
 * @param {Number} id
 */
async function addFriend(url) {
  const currentUser = await getCurrentUser();
  const form = new FormData();
  form.append("symmetric_friends", url)
  const body = { symmetric_friends: url };
  const response = await sendRequest(
    "PATCH",
    `${HOST}/api/users/${currentUser.id}/`,
    form,
    ""
  );
  if (response.ok) {
    const data = await response.json();
    location.reload();
  } else {
    const data = await response.json();
    const alert = createAlert("Could not add friend!", data);

    document.body.prepend(alert);
  }
}

/**
 *
 * @param {string[]} friends
 */
function renderFriends(friends) {
  const container = document.getElementById("div-friend-list");
  friends.forEach(async (friend) => {
    const templateFriendRequest = document.getElementById(
      "template-friend-item"
    );
    const cloneFriendRequest = templateFriendRequest.content.cloneNode(true);

    const divFriendRequest = cloneFriendRequest.querySelector("div");
    const id = friend.split('/').slice(-2)[0]
    const result = await sendRequest("GET", `${HOST}/api/users/${id}/`)
    const profile = await result.json();
    const h5 = divFriendRequest.querySelector("h5");
    h5.textContent = profile.username;

    const a = divFriendRequest.querySelector("a");
    a.href = `profile.html?id=${profile.id}`;

    container.appendChild(cloneFriendRequest);
  });
}

/**
 *
 * @param {{username: String, id: Number, url: String, email: String, phone_number: String, country: String, city: String, street_address: String, bio: String, athletes: []}[]} friends
 */
function renderSearchResult(friends) {
  const container = document.getElementById("div-friend-search-result-list");
  // Need to clear the container before rendering in case of more than one button press
  while (container.firstChild) {
    container.removeChild(container.lastChild);
  }
  friends.forEach((friend) => {
    const templateFriendRequest = document.getElementById(
      "template-friend-search-results"
    );
    const cloneFriendRequest = templateFriendRequest.content.cloneNode(true);

    const divFriendRequest = cloneFriendRequest.querySelector("div");

    const h5 = divFriendRequest.querySelector("h5");
    h5.textContent = friend.username;

    const a = divFriendRequest.querySelector("a");
    a.href = `profile.html?id=${friend.id}`;

    const button = divFriendRequest.querySelector("button");
    button.addEventListener("click", () => addFriend(friend.url));

    container.appendChild(cloneFriendRequest);
  });
}

/**
 *
 */
async function searchFriends() {
  const searchQuery = document.getElementById("input-friend-search").value;
  const result = await fetchSearchResult(searchQuery);
  renderSearchResult(result);
}

window.addEventListener("DOMContentLoaded", async () => {
  const user = await getCurrentUser();
  console.log(user)
  await renderFriends(user.symmetric_friends);
  const searchButton = document.getElementById("btn-search-friends");
  searchButton.addEventListener("click", (event) => searchFriends());
});
