# Generated by Django 3.1 on 2021-03-12 10:36

from django.db import migrations, models
import users.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0012_auto_20210312_1035'),
    ]

    operations = [
        migrations.AlterField(
            model_name='athletefile',
            name='file',
            field=models.FileField(upload_to=users.models.athlete_directory_path),
        ),
    ]
