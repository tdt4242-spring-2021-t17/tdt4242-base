from django.utils import timezone
from rest_framework.test import APIClient, APITestCase


class CommentAPITests(APITestCase):
  def setUp(self):
    self.path = "/api/workouts/"
    self.client = APIClient()
    now = timezone.now()

    # Creating users
    self.alice_r_data = {
      "username": "alice", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path="/api/users/", data=self.alice_r_data)
    self.alice_data = response.data
    self.bob_r_data = {
      "username": "bob", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path="/api/users/", data=self.bob_r_data)
    self.bob_data = response.data

    # Logging in as bob
    self.login_data = {"username": self.bob_r_data["username"], "password": self.bob_r_data["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")

    # Creating an exercise
    response = self.client.post(path="/api/exercises/", data={"description": "Test Exercise", "name": "Test Exercise", "unit": "test unit"})
    self.exerciseA = { "exercise": response.data["url"], "sets": 1, "number": 1}

    # Creating a private workout on bob
    self.data = {
      "name": "wBR", 
      "date": now, 
      "notes": "Good workout", 
      "visibility": "PR",
      "exercise_instances": [self.exerciseA]}
    self.wbr_data = self.client.post(path=self.path, data=self.data, format="json")


    # Logging in as alice
    self.login_data = {"username": self.alice_r_data["username"], "password": self.alice_r_data["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")

    # Creating a private workout as alice
    self.data["name"] = "wAR"
    self.war_data = self.client.post(path=self.path, data=self.data, format="json")
  
  def test_can_comment_on_private_workout(self):
    data = { 
      "workout":  self.wbr_data.data["url"],
      "content": self.login_data["username"]
    }
    response = self.client.post(path="/api/comments/", data=data)
    self.assertEqual(response.status_code, 400)
    data = { 
      "workout":  self.war_data.data["url"],
      "content": self.login_data["username"]
    }
    response = self.client.post(path="/api/comments/", data=data)
    self.assertEqual(response.status_code, 201)
