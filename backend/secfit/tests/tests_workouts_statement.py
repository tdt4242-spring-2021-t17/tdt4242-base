"""
Tests for the workouts application.
"""
from django.http.request import HttpRequest
from django.test import TestCase
from django.utils import timezone

from users.models import User
from workouts.models import Workout
from workouts.permissions import (IsCoachAndVisibleToCoach,
                                  IsCoachOfWorkoutAndVisibleToCoach, IsOwner,
                                  IsOwnerOfWorkout, IsPublic, IsReadOnly,
                                  IsWorkoutPublic)

# Create your tests here.

class WorkoutPermissionsTests(TestCase):
  def setUp(self):
    user = User.objects.create(username="a", email="aa@aa.aa", password="a")
    now = timezone.now()
    Workout.objects.create(
      name="w1", date=now, notes="b", owner=user, visibility="PU")

  ### IsOwner

  def test_is_owner_has_object_permission_accepts_owner(self):
    user = User.objects.create(username="b", email="aa@aa.aa", password="a")
    now = timezone.now()
    workout = Workout.objects.create(
      name="w2", date=now, notes="b", owner=user, visibility="PU")
    req = HttpRequest()
    req.user = user
    
    permission_class = IsOwner()

    result = permission_class.has_object_permission(request=req, view=None, obj=workout)
    self.assertIs(result, True)

  def test_is_owner_has_object_permission_rejects_not_owner(self):
    user = User.objects.create(username="b", email="aa@aa.aa", password="a")
    now = timezone.now()
    workout = Workout.objects.create(
      name="w2", date=now, notes="b", owner=user, visibility="PU")
    userA = User.objects.get(username="a")
    req = HttpRequest()
    req.user = userA
    
    permission_class = IsOwner()

    result = permission_class.has_object_permission(request=req, view=None, obj=workout)
    self.assertIs(result, False)
  
  ### IsOwnerOfWorkout

  def test_is_owner_of_workout_has_permission_owner_accepted(self):
    owner = User.objects.create(username="b", email="aa@aa.aa", password="a")
    now = timezone.now()
    workout = Workout.objects.create(
      name="w2", date=now, notes="b", owner=owner, visibility="PU")
    req = HttpRequest()
    req.user = owner
    req.method = "POST"
    req.data = {"workout": f"/{workout.id}/"}
    
    permission_class = IsOwnerOfWorkout()

    result = permission_class.has_permission(request=req, view=None)
    self.assertIs(result, True)

  def test_is_owner_of_workout_has_permission_other_rejected(self):
    owner = User.objects.create(username="b", email="aa@aa.aa", password="a")
    now = timezone.now()
    workout = Workout.objects.create(
      name="w2", date=now, notes="b", owner=owner, visibility="PU")
    req = HttpRequest()
    other = User.objects.get(username="a")
    req.user = other
    req.method = "POST"
    req.data = {"workout": f"/{workout.id}/"}
    
    permission_class = IsOwnerOfWorkout()

    result = permission_class.has_permission(request=req, view=None)
    self.assertIs(result, False)
  
  def test_is_owner_of_workout_has_permission_get_accepted(self):
    owner = User.objects.create(username="b", email="aa@aa.aa", password="a")
    now = timezone.now()
    workout = Workout.objects.create(
      name="w2", date=now, notes="b", owner=owner, visibility="PU")
    req = HttpRequest()
    other = User.objects.get(username="a")
    req.user = other
    req.method = "GET"
    req.data = {"workout": f"/{workout.id}/"}
    
    permission_class = IsOwnerOfWorkout()

    result = permission_class.has_permission(request=req, view=None)
    self.assertIs(result, True)

  def test_is_owner_of_workout_has_permission_missing_workout_rejected(self):
    req = HttpRequest()
    other = User.objects.get(username="a")
    req.user = other
    req.method = "POST"
    req.data = {}
    
    permission_class = IsOwnerOfWorkout()

    result = permission_class.has_permission(request=req, view=None)
    self.assertIs(result, False)

  def test_is_owner_of_workout_has_object_permission_owner_accepted(self):
    owner = User.objects.create(username="b", email="aa@aa.aa", password="a")
    now = timezone.now()
    workout = Workout.objects.create(
      name="w2", date=now, notes="b", owner=owner, visibility="PU")
    req = HttpRequest()
    req.user = owner
    
    permission_class = IsOwnerOfWorkout()

    class ObjectWithWorkoutField:
      def __init__(self, workout) -> None:
          self.workout = workout
    
    obj = ObjectWithWorkoutField(workout=workout)

    result = permission_class.has_object_permission(request=req, view=None, obj=obj)
    self.assertIs(result, True)

  def test_is_owner_of_workout_has_object_permission_other_rejected(self):
    owner = User.objects.create(username="b", email="aa@aa.aa", password="a")
    now = timezone.now()
    workout = Workout.objects.create(
      name="w2", date=now, notes="b", owner=owner, visibility="PU")
    req = HttpRequest()
    other = User.objects.get(username="a")
    req.user = other
    
    permission_class = IsOwnerOfWorkout()

    class ObjectWithWorkoutField:
      def __init__(self, workout) -> None:
          self.workout = workout
    
    obj = ObjectWithWorkoutField(workout=workout)

    result = permission_class.has_object_permission(request=req, view=None, obj=obj)
    self.assertIs(result, False)

  ### IsCoachAndVisibleToCoach

  def test_is_coach_and_visible_to_coach_coach_accepted_co(self):
    owner = User.objects.create(username="b", email="aa@aa.aa", password="a")
    owner.coach = owner
    req = HttpRequest()
    req.user = owner
    
    permission_class = IsCoachAndVisibleToCoach()

    class ObjectWithOwnerAndVisibilityField:
      def __init__(self, owner, visibility: str) -> None:
          self.owner = owner
          self.visibility = visibility
    
    obj = ObjectWithOwnerAndVisibilityField(owner=owner, visibility="CO")

    result = permission_class.has_object_permission(request=req, view=None, obj=obj)
    self.assertIs(result, True)

  def test_is_coach_and_visible_to_coach_other_rejected(self):
    owner = User.objects.create(username="b", email="aa@aa.aa", password="a")
    owner.coach = owner
    req = HttpRequest()
    other = User.objects.get(username="a")
    req.user = other
    
    permission_class = IsCoachAndVisibleToCoach()

    class ObjectWithOwnerAndVisibilityField:
      def __init__(self, owner, visibility: str) -> None:
          self.owner = owner
          self.visibility = visibility
    
    obj = ObjectWithOwnerAndVisibilityField(owner=owner, visibility="CO")

    result = permission_class.has_object_permission(request=req, view=None, obj=obj)
    self.assertIs(result, False)
  
  def test_is_coach_and_visible_to_coach_coach_rejected_pr(self):
    owner = User.objects.create(username="b", email="aa@aa.aa", password="a")
    owner.coach = owner
    req = HttpRequest()
    other = User.objects.get(username="a")
    req.user = owner
    
    permission_class = IsCoachAndVisibleToCoach()

    class ObjectWithOwnerAndVisibilityField:
      def __init__(self, owner, visibility: str) -> None:
          self.owner = owner
          self.visibility = visibility
    
    obj = ObjectWithOwnerAndVisibilityField(owner=owner, visibility="PR")

    result = permission_class.has_object_permission(request=req, view=None, obj=obj)
    self.assertIs(result, False)
  
  ### IsCoachOfWorkoutAndVisibleToCoach

  def test_is_coach_of_workout_and_visible_to_coach_coach_accepted(self):
    owner = User.objects.create(username="b", email="aa@aa.aa", password="a")
    owner.coach = owner
    now = timezone.now()
    workout = Workout.objects.create(
      name="w2", date=now, notes="b", owner=owner, visibility="CO")
    req = HttpRequest()
    req.user = owner
    
    permission_class = IsCoachOfWorkoutAndVisibleToCoach()

    class ObjectWithWorkoutField:
      def __init__(self, workout) -> None:
          self.workout = workout
    
    obj = ObjectWithWorkoutField(workout=workout)

    result = permission_class.has_object_permission(request=req, view=None, obj=obj)
    self.assertIs(result, True)

  def test_is_coach_of_workout_and_visible_to_coach_coach_accepted(self):
    owner = User.objects.create(username="b", email="aa@aa.aa", password="a")
    owner.coach = owner
    now = timezone.now()
    workout = Workout.objects.create(
      name="w2", date=now, notes="b", owner=owner, visibility="PR")
    req = HttpRequest()
    req.user = owner
    
    permission_class = IsCoachOfWorkoutAndVisibleToCoach()

    class ObjectWithWorkoutField:
      def __init__(self, workout) -> None:
          self.workout = workout
    
    obj = ObjectWithWorkoutField(workout=workout)

    result = permission_class.has_object_permission(request=req, view=None, obj=obj)
    self.assertIs(result, False)

  ### IsPublic

  def test_is_public_pu(self):
    owner = User.objects.create(username="b", email="aa@aa.aa", password="a")
    now = timezone.now()
    workout = Workout.objects.create(
      name="w2", date=now, notes="b", owner=owner, visibility="PU")
    
    permission_class = IsPublic()
    obj = workout

    result = permission_class.has_object_permission(request=None, view=None, obj=obj)
    self.assertIs(result, True)

  def test_is_public_pr(self):
    owner = User.objects.create(username="b", email="aa@aa.aa", password="a")
    now = timezone.now()
    workout = Workout.objects.create(
      name="w2", date=now, notes="b", owner=owner, visibility="PR")
    
    permission_class = IsPublic()
    obj = workout

    result = permission_class.has_object_permission(request=None, view=None, obj=obj)
    self.assertIs(result, False)

  ### IsWorkoutPublic

  def test_is_workout_public_pu(self):
    owner = User.objects.create(username="b", email="aa@aa.aa", password="a")
    now = timezone.now()
    workout = Workout.objects.create(
      name="w2", date=now, notes="b", owner=owner, visibility="PU")
    
    permission_class = IsWorkoutPublic()

    class ObjectWithWorkoutField:
      def __init__(self, workout) -> None:
          self.workout = workout
    
    obj = ObjectWithWorkoutField(workout=workout)

    result = permission_class.has_object_permission(request=None, view=None, obj=obj)
    self.assertIs(result, True)

  def test_is_workout_public_pr(self):
    owner = User.objects.create(username="b", email="aa@aa.aa", password="a")
    now = timezone.now()
    workout = Workout.objects.create(
      name="w2", date=now, notes="b", owner=owner, visibility="PR")
    
    permission_class = IsWorkoutPublic()

    class ObjectWithWorkoutField:
      def __init__(self, workout) -> None:
          self.workout = workout
    
    obj = ObjectWithWorkoutField(workout=workout)

    result = permission_class.has_object_permission(request=None, view=None, obj=obj)
    self.assertIs(result, False)

  ### IsReadOnly

  def test_is_read_only_get(self):
    req = HttpRequest()
    req.method = "GET"
    permission_class = IsReadOnly()
    result = permission_class.has_object_permission(request=req, view=None, obj=None)
    self.assertIs(result, True)
  
  def test_is_read_only_post(self):
    req = HttpRequest()
    req.method = "POST"
    permission_class = IsReadOnly()
    result = permission_class.has_object_permission(request=req, view=None, obj=None)
    self.assertIs(result, False)
