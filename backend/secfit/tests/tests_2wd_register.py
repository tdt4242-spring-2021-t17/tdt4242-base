from abc import ABC
from typing import List

from django.http.response import HttpResponse
from rest_framework.test import APIClient, APITestCase

from users.models import User


class Fields:
  def __init__(self) -> None:
    self.usernames = [Username("alice", True), Username("/", False)]
    self.emails = [Email("bb@bb.bb", True), Email("e", False)]
    self.passwords = [Password("aliceisok", True), Password("e", False)]
    self.phones = [Phone("99999999", True), Phone("e", True), Phone("1", True)]
    self.countries = [Country("France", True), Country("yeetTown3", True), Country("1", True)]
    self.cities = [City("Paris", True)]
    self.street_addresses = [StreetAddress("Road way 2", True)]

class Field(ABC):
  def __init__(self, value: str, valid: bool) -> None:
      self.value = value
      self.valid = valid
      self.name = "field"

class Username(Field):
  def __init__(self, value: str, valid: bool) -> None:
      self.value = value
      self.valid = valid
      self.name = "username"

class Email(Field):
  def __init__(self, value: str, valid: bool) -> None:
      self.value = value
      self.valid = valid
      self.name = "email"

class Password(Field):
  def __init__(self, value: str, valid: bool) -> None:
      self.value = value
      self.valid = valid
      self.name = "password"

class Phone(Field):
  def __init__(self, value: str, valid: bool) -> None:
      self.value = value
      self.valid = valid
      self.name = "phone_number"

class Country(Field):
  def __init__(self, value: str, valid: bool) -> None:
      self.value = value
      self.valid = valid
      self.name = "country"

class City(Field):
  def __init__(self, value: str, valid: bool) -> None:
      self.value = value
      self.valid = valid
      self.name = "city"

class StreetAddress(Field):
  def __init__(self, value: str, valid: bool) -> None:
      self.value = value
      self.valid = valid
      self.name = "street_address"

class UserRegistration2WayDomainTests(APITestCase):

    def setUp(self):
        self.data = {
            "username": "alice", 
            "email": "aa@aa.aa", 
            "password": "aliceisok", 
            "password1": "aliceisok",
            "phone_number": "12345678",
            "country": "",
            "city": "",
            "street_address": ""}
        self.client = APIClient()
        self.fields = Fields()
        self.path = "/api/users/"

    def delete_user(self, id):
        user = User.objects.get(id=id)
        self.client.force_authenticate(user=user)
        self.client.delete(f"{self.path}{id}/")
            

    def create_user(self, first_field: Field, second_field: Field) -> HttpResponse:
        data = self.data.copy()
        if(first_field.name == "password"):
            data["password"] = first_field.value
            data["password1"] = first_field.value
        else:
            data[first_field.name] = first_field.value
        if(second_field.name is not None):
            if(second_field.name == "password"):
                data["password"] = second_field.value
                data["password1"] = second_field.value
            else:
                data[second_field.name] = second_field.value
        response = self.client.post(self.path, data)
        return response

    def handle_create_user(self, first_field: Field, second_field: Field) -> int:
        response = self.create_user(first_field, second_field)
        if response.status_code == 201:
             self.delete_user(response.data["id"])
        return response.status_code

    def test_two_way_domain(self):
        failures = []
        fields: List[List[Field]] = [
            self.fields.usernames, 
            self.fields.emails, 
            self.fields.passwords, 
            self.fields.phones, 
            self.fields.countries, 
            self.fields.cities, 
            self.fields.street_addresses]
        for first_field_list in fields:
            for second_field_list in fields:
                if first_field_list == second_field_list:
                    continue
                for first_field in first_field_list:
                    for second_field in second_field_list:
                        validity = first_field.valid and second_field.valid
                        status_code = self.handle_create_user(first_field, second_field)
                        try: 
                            self.assertEquals((status_code == 201), validity)
                        except AssertionError as e:
                            failures.append({ first_field.name: first_field.value, second_field.name: second_field.value, "error": e })
        self.assertEqual(len(failures), 0)
