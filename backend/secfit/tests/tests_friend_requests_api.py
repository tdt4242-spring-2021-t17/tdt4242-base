from rest_framework.test import APIClient, APITestCase


#@unittest.skip("not finished")
class FriendRequestAPITests(APITestCase):
  def setUp(self):
    self.fr_path = "/friends/friend-requests/"
    self.fl_path = "/friends/friend_list/"
    self.path = "/api/users/"
    self.client = APIClient()

    # Creating users
    self.alice_r_data = {
      "username": "alice", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path="/api/users/", data=self.alice_r_data)
    self.alice_data = response.data
    self.bob_r_data = {
      "username": "bob", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path="/api/users/", data=self.bob_r_data)
    self.bob_data = response.data
    self.clair_r_data = {
      "username": "clair", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path="/api/users/", data=self.clair_r_data)
    self.clair_data = response.data

    # Logging in as alice
    self.login_data = {"username": self.alice_r_data["username"], "password": self.alice_r_data["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")

  def test_can_add_friends(self):
    data = { "symmetric_friends": self.bob_data['id'] }
    response =  self.client.put(path=f"{self.path}{self.alice_data['id']}/", data=data)
    data = { "symmetric_friends": self.clair_data['id'] }
    response =  self.client.put(path=f"{self.path}{self.alice_data['id']}/", data=data)
    self.assertEqual(len(response.data["symmetric_friends"]), 2)
  
  def test_can_add_and_view_friends(self):
    data = { "symmetric_friends": self.bob_data['id'] }
    response =  self.client.put(path=f"{self.path}{self.alice_data['id']}/", data=data)
    data = { "symmetric_friends": self.clair_data['id'] }
    response =  self.client.put(path=f"{self.path}{self.alice_data['id']}/", data=data)
    self.assertEqual(len(response.data["symmetric_friends"]), 2)
    response = self.client.get(path=f"{self.path}{self.alice_data['id']}/")
    self.assertEqual(len(response.data["symmetric_friends"]), 2)

  # def test_can_view_own_friend_list(self):
  #   print("Test: View friend list")
  #   response = self.client.get(path=self.fl_path)
  #   #print(response.__dict__)

  # def test_can_send_friend_request(self):
  #   print("Test: Send friend request")
  #   # As alice
  #   self.data = {"receiver": self.bob_data["url"]}
  #   response = self.client.post(path=self.fr_path, data=self.data)
  #   self.assertEqual(response.status_code, 201)
  #   response = self.client.get(path=self.fr_path)
  #   self.assertEqual(response.data["count"], 1)

  # def test_can_accept_friend_request(self):
  #   print("Test: Accept friend request")
  #   # As alice
  #   self.data = {"receiver": self.bob_data["url"]}
  #   response = self.client.post(path=self.fr_path, data=self.data)
  #   self.assertEqual(response.status_code, 201)
  #   response = self.client.get(path=self.fr_path)
  #   self.assertEqual(response.data["count"], 1)

  #   # Logging in as bob
  #   self.login_data = {"username": self.bob_r_data["username"], "password": self.bob_r_data["password"]}
  #   response = self.client.post(path="/api/token/", data=self.login_data, format="json")
  #   self.access_token = response.data["access"]
  #   self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")

  #   self.data = {"sender": self.alice_data["url"]}
  #   response = self.client.patch(path=self.fr_path + "accept_request/", data=self.data)
  #   print(response.__dict__)
  #   print(response.status_code)
  #   print(response.data)
