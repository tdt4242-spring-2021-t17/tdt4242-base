from PIL import Image
from rest_framework.test import APIClient, APITestCase


class EditProfileAPITests(APITestCase):
  def setUp(self):
    self.path = "/api/users/"
    self.client = APIClient()

    # Creating users
    self.alice_r_data = {
      "username": "alice", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path="/api/users/", data=self.alice_r_data)
    self.alice_data = response.data
    self.bob_r_data = {
      "username": "bob", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path="/api/users/", data=self.bob_r_data)
    self.bob_data = response.data

    # Logging in as alice
    self.login_data = {"username": self.alice_r_data["username"], "password": self.alice_r_data["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")

  def test_upload_image(self):
    img = Image.new(mode="RGB", size=(400, 400), color=(255, 0, 0))
    img.save("test.jpg")
    with open("test.jpg", "rb") as f:
      data = { "profile_image": f }
      response = self.client.put(path=f"{self.path}{self.alice_data['id']}/", data=data)
      self.assertEqual(response.status_code, 200)
      self.assertContains(response=response, text="test.jpg")
    img = Image.new(mode="RGB", size=(400, 400), color=(255, 0, 0))
    img.save("test2.jpg")
    with open("test2.jpg", "rb") as f:
      data = { "profile_image": f }
      response = self.client.put(path=f"{self.path}{self.alice_data['id']}/", data=data)
      self.assertEqual(response.status_code, 200)
      self.assertContains(response=response, text="test2.jpg")

  def test_edit_bio(self):
    data = { "bio": "hello", "athletes": self.alice_data['athletes'] }
    response = self.client.put(path=f"{self.path}{self.alice_data['id']}/", data=data)
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data["bio"], "hello")
