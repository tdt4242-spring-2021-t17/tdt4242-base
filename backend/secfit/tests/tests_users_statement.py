from typing import Dict

from django.db.models.base import Model
from django.db.utils import IntegrityError
from django.test import TestCase
from rest_framework.exceptions import ValidationError

from users.models import AthleteFile, Offer, User
from users.serializers import (AthleteFileSerializer, OfferSerializer,
                               UserPutSerializer, UserSerializer)

# Create your tests here.

class UserSerializerTests(TestCase):
  def setUp(self):
    username = "testA"
    email = "aa@aa.aa"
    password = "a"
    User.objects.create(username=username, email=email, password=password)
    username = "testB"
    User.objects.create(username=username, email=email, password=password)

  def test_can_create_user(self):
    user_ser = UserSerializer()
    validated_data: Dict = { 
      "username": "a", 
      "email": "aa@aa.aa", 
      "password": "a", 
      "phone_number": "",
      "country": "",
      "city": "",
      "street_address": ""}
    model = user_ser.create(validated_data=validated_data)
    self.assertIsInstance(model, Model)

  def test_validate_password_rejects_short_password(self):
    """Should fail, there is password length validation"""
    data = {"password": "rrrrrrr", "password1": "rrrrrrr"}
    user_ser = UserSerializer(instance=None, data=data)
    self.assertRaises(ValidationError, user_ser.validate_password, "valid")
  
  def test_validate_password_rejects_numeric_password(self):
    """Should fail, there is no numeric password restriction"""
    data = {"password": "0176038745", "password1": "0176038745"}
    user_ser = UserSerializer(instance=None, data=data)
    self.assertRaises(ValidationError, user_ser.validate_password, "valid")

  def test_validate_password_rejects_common_password(self):
    """Should fail, there is no common password restriction"""
    data = {"password": "password1", "password1": "password1"}
    user_ser = UserSerializer(instance=None, data=data)
    self.assertRaises(ValidationError, user_ser.validate_password, "valid")

  def test_validate_password_rejects_different_passwords(self):
    """Should fail, there is no check for equal passwords"""
    data = {"password": "grungedunge", "password1": "grungedung"}
    user_ser = UserSerializer(instance=None, data=data)
    self.assertRaises(ValidationError, user_ser.validate_password, "valid")
  
  def test_validate_password_accepts_equal_passwords(self):
    data = {"password": "grungedunge", "password1": "grungedunge"}
    user_ser = UserSerializer(instance=None, data=data)
    self.assertEqual(user_ser.validate_password("valid"), "valid")
  
  def test_can_update_user_bio(self):
    user_ser = UserSerializer()
    validated_data: Dict = { 
      "username": "a", 
      "email": "aa@aa.aa", 
      "password": "a", 
      "phone_number": "",
      "country": "",
      "city": "",
      "street_address": ""}
    instance = user_ser.create(validated_data=validated_data)
    user_put_ser = UserPutSerializer()
    validated_data: Dict = {
      "bio": "yes"
    }
    result = user_put_ser.update(instance=instance, validated_data=validated_data)
    self.assertEqual(result.bio, "yes")
  
  def test_can_update_user_athletes(self):
    user_ser = UserSerializer()
    validated_data: Dict = { 
      "username": "a", 
      "email": "aa@aa.aa", 
      "password": "a", 
      "phone_number": "",
      "country": "",
      "city": "",
      "street_address": ""}
    instance = user_ser.create(validated_data=validated_data)
    user_put_ser = UserPutSerializer()
    validated_data: Dict = {
      "athletes": [instance]
    }
    result = user_put_ser.update(instance=instance, validated_data=validated_data)
    self.assertEqual(result.coach_id, instance.id)


  
  def test_can_not_create_user_with_existing_username(self):
    user_ser = UserSerializer()
    validated_data: Dict = { 
      "username": "testA", 
      "email": "aa@aa.aa", 
      "password": "a", 
      "phone_number": "",
      "country": "",
      "city": "",
      "street_address": ""}
    self.assertRaisesMessage(
      IntegrityError, 
      "UNIQUE constraint failed: users_user.username", 
      user_ser.create, 
      validated_data=validated_data)

  ### AthleteFileSerializer

  def test_can_upload_athlete_file(self):
    ath_ser = AthleteFileSerializer()
    username = "testA"
    user = User.objects.get(username=username)
    validated_data: Dict = { 
      "id": 2, 
      "owner": user, 
      "file": "",
      "athlete": user}
    result = ath_ser.create(validated_data)
    self.assertIsInstance(result, AthleteFile)
    

  ### OfferSerializer

  def test_can_create_offer_to_self(self):
    off_ser = OfferSerializer()
    username = "testA"
    user = User.objects.get(username=username)
    validated_data: Dict = { 
      "id": 2, 
      "owner": user,
      "recipient": user,
      "status": "p"
      }
    result = off_ser.create(validated_data=validated_data)
    self.assertIsInstance(result, Offer)
  
  def test_can_create_offer_to_other(self):
    off_ser = OfferSerializer()
    username = "testA"
    userA = User.objects.get(username=username)
    username = "testB"
    userB = User.objects.get(username=username)
    validated_data: Dict = { 
      "id": 2, 
      "owner": userA,
      "recipient": userB,
      "status": "p"
      }
    result = off_ser.create(validated_data=validated_data)
    self.assertIsInstance(result, Offer)
