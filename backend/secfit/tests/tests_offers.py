from django.utils import timezone
from rest_framework.test import APIClient, APITestCase


def pretty_print_response(response):
  print(response.data)
  print(response.status_code)

class OffersAPITests(APITestCase):
  def setUp(self):
    self.path = "/api/offers/"
    self.client = APIClient()

    # Creating users
    self.alice_r_data = {
      "username": "alice", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path="/api/users/", data=self.alice_r_data)
    self.alice_data = response.data

    self.bob_r_data = {
      "username": "bob", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path="/api/users/", data=self.bob_r_data)
    self.bob_data = response.data

    self.clair_r_data = {
      "username": "clair", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path="/api/users/", data=self.clair_r_data)
    self.clair_data = response.data

    # Logging in as bob
    self.login_data = {"username": self.bob_r_data["username"], "password": self.bob_r_data["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")

    # Making offer to clair
    response = self.client.post(path=self.path, data={"status": "p", "recipient": self.clair_data["url"]})
    self.b_to_c_offer = response.data

    # Logging in as alice
    self.login_data = {"username": self.alice_r_data["username"], "password": self.alice_r_data["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")
  
  ## GET

  def test_can_not_view_other_user_offer(self):
    response = self.client.get(path=f"{self.path}{self.b_to_c_offer['id']}/")
    self.assertEqual(response.status_code, 400)
  
  def test_can_view_own_offer_recipient(self):
    # Logging in as clair
    self.login_data = {"username": self.clair_r_data["username"], "password": self.clair_r_data["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")

    response = self.client.get(path=f"{self.path}{self.b_to_c_offer['id']}/")
    self.assertEqual(response.status_code, 200)
  
  def test_can_view_own_offer_owner(self):
    # Logging in as bob
    self.login_data = {"username": self.bob_r_data["username"], "password": self.bob_r_data["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")

    response = self.client.get(path=f"{self.path}{self.b_to_c_offer['id']}/")
    self.assertEqual(response.status_code, 200)
  
  ## PATCH/PUT

  def test_can_not_accept_other_offer(self):
    data = {"status": "a"}
    response = self.client.patch(path=f"{self.path}{self.b_to_c_offer['id']}/", data=data)
    self.assertEqual(response.status_code, 400)
    data = {
      'url': 'http://testserver/api/offers/1/', 
      'id': 1, 
      'owner': 'bob', 
      'recipient': 'http://testserver/api/users/3/', 
      'status': 'a', 
      'timestamp': '2021-03-22T10:06:24.034403Z'}
    response = self.client.put(path=f"{self.path}{self.b_to_c_offer['id']}/", data=data)
    self.assertEqual(response.status_code, 400)

  def test_can_accept_own_offer_recipient(self):
    # Logging in as clair
    self.login_data = {"username": self.clair_r_data["username"], "password": self.clair_r_data["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")

    data = {"status": "a"}
    response = self.client.patch(path=f"{self.path}{self.b_to_c_offer['id']}/", data=data)
    self.assertEqual(response.status_code, 200)

  ## DELETE

  def test_can_not_delete_other_user_offer(self):
    response = self.client.delete(path=f"{self.path}{self.b_to_c_offer['id']}/")
    self.assertEqual(response.status_code, 400)

  def test_can_delete_own_user_offer_owner(self):
    # Logging in as bob
    self.login_data = {"username": self.bob_r_data["username"], "password": self.bob_r_data["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")

    response = self.client.delete(path=f"{self.path}{self.b_to_c_offer['id']}/")
    self.assertEqual(response.status_code, 204)

  def test_can_not_delete_own_user_offer_recipient(self):
    # Logging in as clair
    self.login_data = {"username": self.clair_r_data["username"], "password": self.clair_r_data["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")

    response = self.client.delete(path=f"{self.path}{self.b_to_c_offer['id']}/")
    self.assertEqual(response.status_code, 400)
