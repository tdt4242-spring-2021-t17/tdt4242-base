from django.test.client import RequestFactory
from rest_framework.test import APIClient, APITestCase

from users.views import RememberMe


def pretty_print_response(response):
  print(response.data)
  print(response.status_code)

class OffersAPITests(APITestCase):
  def setUp(self):
    self.path = "/api/offers/"
    self.client = APIClient()

    # Creating users
    self.alice_r_data = {
      "username": "alice", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path="/api/users/", data=self.alice_r_data)
    self.alice_data = response.data


  def test_can_get_rememberme_token(self):
      # Logging in as alice
    self.login_data = {"username": self.alice_r_data["username"], "password": self.alice_r_data["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")
    response = self.client.get(path="/api/remember_me/")
    self.assertGreater(len(response.data["remember_me"]), 0)

  def test_can_post_rememberme(self):
      # Logging in as alice
    self.login_data = {"username": self.alice_r_data["username"], "password": self.alice_r_data["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")
    response = self.client.get(path="/api/remember_me/")

    request = RequestFactory().post('/api/remember_me/')
    request.COOKIES['remember_me'] = response.data["remember_me"]
    response = RememberMe.as_view()(request)
    self.assertGreater(len(response.data["access"]), 0)
    self.assertGreater(len(response.data["refresh"]), 0)
