from rest_framework.test import APIClient, APITestCase


class UserRegistrationTests(APITestCase):
  def setUp(self):
    self.path = "/api/users/"
    self.client = APIClient()

  ### 2-way domain tests

  def test_register_different_passwords_fails(self):
    """Should fail, there is no check for different passwords"""
    data = {
      "username": "alice", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisOk",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path=self.path, data=data)
    self.assertEqual(response.status_code, 400)
  
  def test_register_bad_request_fails(self):
    data = {
      "username": "alice", 
      "email": "aa@aa.aa", 
      "password": "aliceisok"}
    response = self.client.post(path=self.path, data=data)
    self.assertEqual(response.status_code, 400)
  
  def test_register_bad_email_fails(self):
    data = {
      "username": "alice", 
      "email": "a@a.a", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path=self.path, data=data)
    self.assertEqual(response.status_code, 400)
  
  ### Boundary tests
  
  def test_register_password_7_fails(self):
    """Should fail if there is password length validation"""
    data = {
      "username": "alice", 
      "email": "aa@aa.aa", 
      "password": "aliceok", 
      "password1": "aliceok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path=self.path, data=data)
    self.assertEqual(response.status_code, 400)

  def test_register_password_8_ok(self):
    data = {
      "username": "alice", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path=self.path, data=data)
    self.assertEqual(response.status_code, 201)

  # def test_register_phone_number_7_fails(self):
  #   """Should fail if there is no phone validation"""
  #   data = {
  #     "username": "alice", 
  #     "email": "aa@aa.aa", 
  #     "password": "aliceisok", 
  #     "password1": "aliceisok",
  #     "phone_number": "1234567",
  #     "country": "",
  #     "city": "",
  #     "street_address": ""}
  #   response = self.client.post(path=self.path, data=data)
  #   self.assertEqual(response.status_code, 400)

  def test_register_phone_number_8_ok(self):
    data = {
      "username": "alice", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path=self.path, data=data)
    self.assertEqual(response.status_code, 201)

  def test_register_username_151_length_fails(self):
    data = {
      "username": "a"*151, 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path=self.path, data=data)
    self.assertEqual(response.status_code, 400)

  def test_register_username_150_length_ok(self):
    data = {
      "username": "a"*150, 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path=self.path, data=data)
    self.assertEqual(response.status_code, 201)
