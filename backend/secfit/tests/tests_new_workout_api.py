from django.db.utils import IntegrityError
from django.utils import timezone
from rest_framework.test import APIClient, APITestCase


class WorkoutCreationTests(APITestCase):
  def setUp(self):
    self.path = "/api/workouts/"
    self.client = APIClient()
    self.now = timezone.now()

    self.registration_data = {
      "username": "alice", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    self.client.post(path="/api/users/", data=self.registration_data)
    self.other_reg = {
      "username": "bob", 
      "email": "aa@aa.aa", 
      "password": "aliceisok", 
      "password1": "aliceisok",
      "phone_number": "12345678",
      "country": "",
      "city": "",
      "street_address": ""}
    response = self.client.post(path="/api/users/", data=self.other_reg)

    self.login_data = {"username": self.other_reg["username"], "password": self.other_reg["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")

    response = self.client.post(path="/api/exercises/", data={"description": "Test Exercise", "name": "Test Exercise", "unit": "test unit"})
    self.exerciseA = { "exercise": response.data["url"], "sets": 1, "number": 1}

    self.data = {
      "name": "wBR", 
      "date": self.now, 
      "notes": "Good workout", 
      "visibility": "PR",
      "exercise_instances": [self.exerciseA]}

    response = self.client.post(path=self.path, data=self.data, format="json")
    self.data["name"] = "wBU"
    self.data["visibility"] = "PU"
    response = self.client.post(path=self.path, data=self.data, format="json")
    self.num_workouts = 2
    self.pr_workouts = 1
    self.pu_workouts = 1

    self.data = {
      "name": "wA", 
      "date": self.now, 
      "notes": "Good workout", 
      "visibility": "PU",
      "exercise_instances": [self.exerciseA]}

    self.login_data = {"username": self.registration_data["username"], "password": self.registration_data["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")

  def pretty_print_response(self, response):
    if "status_code" in response.__dict__:
      print(response.status_code)
    if "data" in response.__dict__:
      print(response.data)

  ### Black box FR5

  def test_create_workout_normal_ok(self):
    response = self.client.post(path=self.path, data=self.data, format="json")
    self.assertEqual(response.status_code, 201)
    response = self.client.get(path=f"{self.path}")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data["count"], self.pu_workouts + 1)

  def test_create_workout_can_view_own_private_workout_list(self):
    self.data["visibility"] = "PR"
    response = self.client.post(path=self.path, data=self.data, format="json")
    self.assertEqual(response.status_code, 201)
    response = self.client.get(path=f"{self.path}")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data["count"], self.pu_workouts + 1)
  
  def test_create_workout_can_not_view_others_private_workout_list(self):
    response = self.client.get(path=f"{self.path}")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data["count"], self.pu_workouts)
  
  def test_create_workout_can_view_public_workout_list(self):
    response = self.client.get(path=f"{self.path}")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data["count"], self.pu_workouts)
  
  def test_athlete_can_not_see_others_co_workout(self):
    self.data["visibility"] = "CO"
    response = self.client.post(path=self.path, data=self.data, format="json")
    self.assertEqual(response.status_code, 201)

    self.login_data = {"username": self.other_reg["username"], "password": self.other_reg["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")

    response = self.client.get(path=f"{self.path}")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data["count"], self.num_workouts)

  def test_coach_can_see_others_co_workout(self):
    response = self.client.get(path=f"/api/users/bob/")
    response = self.client.patch(path=f"/api/users/{response.data['id']}/", data={"coach": response.data["url"]}, format="json")

    self.data["visibility"] = "CO"
    response = self.client.post(path=self.path, data=self.data, format="json")
    self.assertEqual(response.status_code, 201)
    
    self.login_data = {"username": self.other_reg["username"], "password": self.other_reg["password"]}
    response = self.client.post(path="/api/token/", data=self.login_data, format="json")
    self.access_token = response.data["access"]
    self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.access_token}")
    
    response = self.client.get(path=f"{self.path}")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data["count"], self.pu_workouts + 1)

  
  ### Boundary tests

  # Name

  def test_create_workout_name_99_ok(self):
    self.data["name"] = "n"*99
    response = self.client.post(path=self.path, data=self.data, format="json")
    self.assertEqual(response.status_code, 201)
    response = self.client.get(path=f"{self.path}")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data["count"], self.pu_workouts + 1)

  def test_create_workout_name_100_ok(self):
    self.data["name"] = "n"*100
    response = self.client.post(path=self.path, data=self.data, format="json")
    self.assertEqual(response.status_code, 201)
    response = self.client.get(path=f"{self.path}")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data["count"], self.pu_workouts + 1)
  
  def test_create_workout_name_101_fails(self):
    self.data["name"] = "n"*101
    response = self.client.post(path=self.path, data=self.data, format="json")
    self.assertEqual(response.status_code, 400)
    response = self.client.get(path=f"{self.path}")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data["count"], self.pu_workouts)

  # Visibility

  def test_create_workout_visibility_3_fails(self):
    self.data["visibility"] = "PUU"
    response = self.client.post(path=self.path, data=self.data, format="json")
    self.assertEqual(response.status_code, 400)
    response = self.client.get(path=f"{self.path}")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data["count"], self.pu_workouts)

  def test_create_workout_visibility_1_fails(self):
    self.data["visibility"] = "P"
    response = self.client.post(path=self.path, data=self.data, format="json")
    self.assertEqual(response.status_code, 400)
    response = self.client.get(path=f"{self.path}")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data["count"], self.pu_workouts)
  
  # Sets

  def test_create_workout_sets_minus_1_fails(self):
    self.data["exercise_instances"][0]["sets"] = -1
    self.assertRaises(IntegrityError, self.client.post, path=self.path, data=self.data, format="json")
  
  def test_create_workout_sets_0_ok(self):
    self.data["exercise_instances"][0]["sets"] = 0
    response = self.client.post(path=self.path, data=self.data, format="json")
    self.assertEqual(response.status_code, 201)
    response = self.client.get(path=f"{self.path}")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data["count"], self.pu_workouts + 1)

  # Number

  def test_create_workout_number_minus_1_fails(self):
    self.data["exercise_instances"][0]["number"] = -1
    self.assertRaises(IntegrityError, self.client.post, path=self.path, data=self.data, format="json")
  
  def test_create_workout_number_0_ok(self):
    self.data["exercise_instances"][0]["number"] = 0
    response = self.client.post(path=self.path, data=self.data, format="json")
    self.assertEqual(response.status_code, 201)
    response = self.client.get(path=f"{self.path}")
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.data["count"], self.pu_workouts + 1)
  
  # Regression tests

  def test_create_workout_then_update(self):
    data = {
      "name": "wBF",
      "date": self.now,
      "notes": "Good workout",
      "visibility": "PR",
      "exercise_instances": [self.exerciseA],
      "files": []}
    response = self.client.post(self.path, data=data, format="json")
    self.assertEqual(response.status_code, 201)
    data["notes"] = "Bad workout"
    response = self.client.put(f"{self.path}{response.data['id']}/", data=data, format="json")
    self.assertEqual(response.status_code, 200)
