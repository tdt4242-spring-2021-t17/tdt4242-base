# Todo

## X Requirement "Add other users as friends"

### Backend

Model: Friends field and friendship relation
Serializer: Friend stuff?
View: Get friends and put friends on UserDetails in update. Get friends and friend requests.
Url: Nothing new?

### Frontend

Mainly good now, just need backend and requests

- Friends page
  - Search for friends (requirement below)
  - Friend requests list
  - Friend list

## X Requirement "Search for other users by name"

Done

### Backend

View: Either parameter on get on UserList or new method
Urls: New endpoint with search parameter: /api/users/?search=

### Frontend

- Search field on Friends page with search button
- Result list after click, or "No results"
- Button for adding the friend next to result in result list

## X Requirement "Create a personal profile"

Done

### Backend

No modifications needed?

### Frontend

Done, except showing real image

- Profile page
  - Show name
  - Show image or placeholder
  - Show email
  - Show address

## X Requirement "Edit biographical text on personal profile"

Done

### Backend

Models: Add bio field to User
Serializers: Bio field
Views: In UserDetails, in put, allow bio updates
Urls: Endpoint for changing bio

### Frontend

- Depends on above requirement
- Show bio
- Click edit next to bio to edit it
- Click save to do a put to the api

## X Requirement "Edit personal profile image"

### Backend

Find out: Images on Django, how the fuck. Maybe look at the file upload functionality
Models: Image on User somehow
Serializers: How get image pls
Views: Include image on get UserDetails

### Frontend

- Depends on Create personal profile
- Edit button next to profile image on Profile page
- Upload image thingy, html you know
- Do a put to user endpoint /api/users/:id/?image= or something

## X Requirement "View a user’s personal profile"

Done

### Backend

No modifications?

### Frontend

- Depends on Create personal profile
- Links for user profiles, /profile.html?user=
- Load user with id
- Add link wherever a user's name is shown:
  - Athletes page
  - Comments on Workout page
  - List on Workouts page
  - Coach page
  - Friends page
    - Friend list
    - Friend request list
    - Results on search

## X Requirement "Get notified when friends log new workouts"

### Backend

Models: Viewed by field on Workout? Or viewed workouts on User?
Serializers: Find number of unseen workouts for a user somehow
Views: Include number of unseen workouts on get on UserDetails. Put on WorkoutDetails to update seen status.
Urls: Endpoint for updating seen status on workout

### Frontend

- On each page load, when getting current user, get the unseen workouts number
- Add a little badge on the Workouts link showing the number
- When loading Workouts page, update seen status by api

## X Requirement "Filter list of previous workouts by friends"

### Backend

No modifications needed if done client side

### Frontend

- Add tab for "Friends' workouts" on Workouts page
- Add switch case to remove workouts by users not in friend list
